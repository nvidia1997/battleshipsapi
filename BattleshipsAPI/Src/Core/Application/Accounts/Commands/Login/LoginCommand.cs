﻿using MediatR;

namespace Application.Accounts.Commands.Login
{
    public class LoginCommand : IRequest<LoginVm>
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
