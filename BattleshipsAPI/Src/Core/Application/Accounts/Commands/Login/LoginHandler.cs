﻿using Application.Accounts.Commands.Logout;
using Application.Common.Exceptions;
using Application.Common.Interfaces.Infrastructure.Infrastructure.Services;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Accounts.Commands.Login
{
    public class LoginHandler : IRequestHandler<LoginCommand, LoginVm>
    {
        private readonly IUserManagerService _userManagerService;
        private readonly IConfiguration _configuration;
        private readonly IMediator _mediator;

        public LoginHandler(IUserManagerService userManagerService, IMediator mediator, IConfiguration configuration)
        {
            this._userManagerService = userManagerService ?? throw new ArgumentNullException(nameof(userManagerService));
            this._configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            this._mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public async Task<LoginVm> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            await _mediator.Send(new LogoutCommand());

            var loginResult = await _userManagerService.LoginAsync(request.Username, request.Password);

            if (!loginResult.Succeeded)
            {
                throw new GeneralException(new Errors.ErrorDto { Code = "WrongCredentials" });
            }

            var jwt = GenerateJWT();
            var vm = new LoginVm()
            {
                Username = request.Username,
                JWT = jwt
            };

            return vm;
        }

        private string GenerateJWT()
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
              _configuration["Jwt:Issuer"],
              _configuration["Jwt:Issuer"],
              null,
              expires: DateTime.Now.AddHours(2),
              signingCredentials: credentials
              );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
