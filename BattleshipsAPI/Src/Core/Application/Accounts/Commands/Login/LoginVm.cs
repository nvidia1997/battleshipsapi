﻿namespace Application.Accounts.Commands.Login
{
    public class LoginVm
    {
        public string Username { get; set; }
        public string JWT { get; set; }
    }
}
