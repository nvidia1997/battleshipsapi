﻿using Application.Common.Interfaces.Infrastructure.Infrastructure.Services;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Accounts.Commands.Logout
{
    public class LogoutHandler : IRequestHandler<LogoutCommand, Unit>
    {
        private readonly IUserManagerService userManagerService;

        public LogoutHandler(IUserManagerService userManagerService)
        {
            this.userManagerService = userManagerService ?? throw new ArgumentNullException(nameof(userManagerService));
        }

        public async Task<Unit> Handle(LogoutCommand request, CancellationToken cancellationToken)
        {
            await userManagerService.Logout();

            return Unit.Value;
        }
    }
}
