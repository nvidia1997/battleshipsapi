﻿using MediatR;


namespace Application.Accounts.Commands.Register
{
    public class RegisterCommand : IRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string ConfirmedPassword { get; set; }
    }
}
