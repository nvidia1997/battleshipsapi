﻿using Application.Common.Exceptions;
using Application.Common.Interfaces.Infrastructure.Infrastructure.Services;
using Application.Errors;
using Domain.Entities.Infrastructure;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Accounts.Commands.Register
{
    public class RegisterHandler : IRequestHandler<RegisterCommand, Unit>
    {
        private readonly IUserManagerService _userManagerService;

        public RegisterHandler(IUserManagerService userManagerService)
        {
            this._userManagerService = userManagerService ?? throw new ArgumentNullException(nameof(userManagerService));
        }

        public async Task<Unit> Handle(RegisterCommand request, CancellationToken cancellationToken)
        {
            var applicationUser = new ApplicationUser()
            {
                UserName = request.Username,
                Email = request.Username,
            };

            if (_userManagerService.Contains(applicationUser))
            {
                throw new GeneralException(new ErrorDto { Code = "DuplicatedUser" });
            }

            var result = await _userManagerService.CreateUserAsync(applicationUser, request.Password);

            if (!result.Succeeded)
            {
                throw new GeneralException()
                {
                    Errors = result.Errors.Select(e => new ErrorDto(e.Code, e.Description)),
                };
            }

            return Unit.Value;
        }
    }
}
