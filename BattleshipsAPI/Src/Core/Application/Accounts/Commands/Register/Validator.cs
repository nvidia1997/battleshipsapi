﻿using FluentValidation;

namespace Application.Accounts.Commands.Register
{
    public class Validator : AbstractValidator<RegisterCommand>
    {
        public Validator()
        {
            RuleFor(x => x.Username)
               .EmailAddress()
               .WithMessage($"Invalid email");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Passwords is empty");

            RuleFor(x => x.Password)
                .Equal(x => x.ConfirmedPassword)
                .WithMessage("Passwords do not match");
        }
    }
}
