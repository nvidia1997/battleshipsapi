﻿using Application.BoardCells;
using System.Collections.Generic;

namespace Application.Boards
{
    public class BoardDto
    {
        public int Id { get; set; }
        public int SizeX { get; set; }
        public int SizeY { get; set; }
        public List<BoardCellDto> Cells { get; set; }
    }
}
