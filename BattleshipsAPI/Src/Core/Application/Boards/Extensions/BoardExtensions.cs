﻿using Domain.Entities.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Application
{
    internal static class BoardExtensions
    {
        internal static List<BoardCell> GenerateCells(this Board board)
        {
            var cells = new List<BoardCell>();

            for (int x = 0; x <= board.Size; x++)
            {
                for (int y = 0; y <= board.Size; y++)
                {
                    cells.Add(new BoardCell() { X = x, Y = y });
                }
            }

            return cells;
        }

        internal static void RandomizeShipLocations(this Board board)
        {
            var shipSizes = GetShipSizes();

            foreach (var shipSize in shipSizes)
            {
                TrySetShip(board, shipSize);
            }
        }

        /// <summary>
        /// Creates an array with lengths of each ship
        /// </summary>
        /// <returns></returns>
        private static int[] GetShipSizes()
        {
            return new int[] { 5, 4, 3, 3, 2 };
        }

        private static void TrySetShip(this Board board, int shipSize)
        {
            var random = new Random();

            var randomStartX = random.Next(0, board.Size);
            var randomStartY = random.Next(0, board.Size);

            var direction = random.Next(0, 10);
            IEnumerable<BoardCell> tempSelectedCells;

            if (direction <= 5)
            {
                //vertical
                var isUpDirection = direction <= 2;

                if (isUpDirection)
                {
                    tempSelectedCells = board.Cells.Where(c => c.X == randomStartX && c.Y >= randomStartY && !c.IsShipPart);
                }
                else
                {
                    tempSelectedCells = board.Cells.Where(c => c.X == randomStartX && c.Y <= randomStartY && !c.IsShipPart);
                }

            }
            else
            {
                //horizontal
                var isLeftDirection = direction >= 7;

                if (isLeftDirection)
                {
                    tempSelectedCells = board.Cells.Where(c => c.X <= randomStartX && c.Y == randomStartY && !c.IsShipPart);
                }
                else
                {
                    tempSelectedCells = board.Cells.Where(c => c.X >= randomStartX && c.Y == randomStartY && !c.IsShipPart);
                }
            }

            var selectedCells = tempSelectedCells.Take(shipSize).ToList();

            if (selectedCells.Count == shipSize && selectedCells.Count >= GetShipSizes().Min())
            {
                if (selectedCells.HasSequentialOrder())
                {
                    selectedCells.ForEach((cell) => { cell.IsShipPart = true; });
                    return;
                }
            }

            TrySetShip(board, shipSize);
        }

        private static bool HasSequentialOrder(this List<BoardCell> cells)
        {
            for (int i = 1; i < cells.Count; i++)
            {
                var prevCell = cells[i - 1];
                var currentCell = cells[i];
                if (!(currentCell.X == prevCell.X + 1 || currentCell.X == prevCell.X - 1 || currentCell.Y == prevCell.Y + 1 || currentCell.Y == prevCell.Y - 1))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
