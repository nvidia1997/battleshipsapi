﻿using Application.Common.Exceptions;
using Application.Errors;
using FluentValidation;
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Common.Behaviours
{
    public class RequestValidation<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRequest<TResponse>
    {
        private readonly IValidator<TRequest> validator;

        public RequestValidation(IValidator<TRequest> validator)
        {
            this.validator = validator;
        }

        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            try
            {
                validator?.ValidateAndThrow(request);
                return next();
            }
            catch (ValidationException validationException)
            {
                throw new GeneralException()
                {
                    Errors = validationException.Errors?.Select(e => new ErrorDto(e.ErrorCode, e.ErrorMessage))
                };
            }
        }
    }
}
