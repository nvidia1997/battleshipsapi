﻿using Application.Errors;
using System;
using System.Collections.Generic;
using System.Net;

namespace Application.Common.Exceptions
{
    public class GeneralException : Exception
    {
        public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.BadRequest;
        public IEnumerable<ErrorDto> Errors { get; set; }

        public GeneralException(params ErrorDto[] errors)
        {
            this.Errors = new List<ErrorDto>(errors);
        }
    }
}
