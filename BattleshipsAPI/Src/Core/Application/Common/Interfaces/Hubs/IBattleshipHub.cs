﻿using Application.Servers;
using System;
using System.Threading.Tasks;

namespace Application.Common.Interfaces.Hubs
{
    public interface IBattleshipHub
    {
        Task EmitServerCreated(ShortServerVm serverVm);
        Task JoinServer(int serverId);
        Task EmitServersUpdated(ServerVm serverVm, ShortServerVm shortServerVm);
        Task EmitServerDeleted(ServerVm serverVm);
        Task EmitServerUpdated(ServerVm serverVm);
        Task EmitShortServerUpdated(ShortServerVm shortServerVm);
        Task LeaveServer(int serverId);
        Task OnDisconnectedAsync(Exception exception);
    }
}
