﻿using System;
using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IUnitOfWork<Type> : IDisposable
        where Type : class
    {
        Task<int> Complete();
    }
}
