﻿namespace Application.Common.Interfaces.Infrastructure.Infrastructure.Services
{
    public interface ICryptographyProcessor
    {
        string GenerateHash(string input, string salt);
    }
}
