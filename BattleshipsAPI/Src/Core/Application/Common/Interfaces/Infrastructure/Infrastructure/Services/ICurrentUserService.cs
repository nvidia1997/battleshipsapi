﻿namespace Application.Common.Interfaces.Infrastructure.Infrastructure.Services
{
    public interface ICurrentUserService
    {
        string Username { get; }
        string Email { get; }
        string UserId { get; }
        bool IsAuthenticated { get; }
    }
}
