﻿using Domain.Entities.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Application.Common.Interfaces.Infrastructure.Infrastructure.Services
{
    public interface IUserManagerService
    {
        Task<IdentityResult> CreateUserAsync(ApplicationUser user, string password);

        bool Contains(ApplicationUser user);

        Task<ApplicationUser> FindByUsernameAsync(string username);

        Task<ApplicationUser> FindByIdAsync(string userId);

        Task<SignInResult> LoginAsync(string username, string password, bool isPersistent = false);

        Task Logout();
    }
}
