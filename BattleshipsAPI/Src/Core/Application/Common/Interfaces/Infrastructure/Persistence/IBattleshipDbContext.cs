﻿using Domain.Entities.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Application.Common.Interfaces.Infrastructure.Persistence
{
    public interface IBattleshipDbContext
    {
        DbSet<Board> Boards { get; set; }
        DbSet<BoardCell> BoardCells { get; set; }
        DbSet<Server> Servers { get; set; }
        DbSet<Player> Players { get; set; }
    }
}
