﻿using Application.Common.Interfaces.Infrastructure.Persistence.Repositories;

namespace Application.Common.Interfaces.Infrastructure.Persistence
{
    public interface IPersistenceUnitOfWork : IUnitOfWork<IPersistenceUnitOfWork>
    {
        IBoardsRepository Boards { get; }
        IBoardCellsRepository BoardCells { get; }
        IPlayersRepository Players { get; }
        IServersRepository Servers { get; }
    }
}
