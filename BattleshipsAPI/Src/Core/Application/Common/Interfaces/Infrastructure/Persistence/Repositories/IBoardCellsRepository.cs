﻿using Domain.Entities.Persistence;

namespace Application.Common.Interfaces.Infrastructure.Persistence.Repositories
{
    public interface IBoardCellsRepository : IRepository<BoardCell>
    {
    }
}
