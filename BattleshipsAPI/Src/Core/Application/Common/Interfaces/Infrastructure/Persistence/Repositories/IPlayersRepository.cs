﻿using Domain.Entities.Persistence;
using System;
using System.Linq;

namespace Application.Common.Interfaces.Infrastructure.Persistence.Repositories
{
    public interface IPlayersRepository : IRepository<Player>
    {
        IQueryable<Player> FindWithAllProps(Func<Player, bool> predicate);

        Player FirstOrDefaultWithAllProps(Func<Player, bool> predicate);
    }
}
