﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Application.Common.Interfaces.Infrastructure.Persistence.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);

        public void AddRange(TEntity[] entities);

        void Remove(TEntity entity);

        bool Contains(Func<TEntity, bool> predicate);

        TEntity FirstOrDefault(Func<TEntity, bool> predicate, params Expression<Func<TEntity, object>>[] properties);

        IQueryable<TEntity> Find(Func<TEntity, bool> predicate, params Expression<Func<TEntity, object>>[] properties);

        IQueryable<TEntity> GetAll();

        int Count(Func<TEntity, bool> predicate);

        public void Update(TEntity entity);
    }
}
