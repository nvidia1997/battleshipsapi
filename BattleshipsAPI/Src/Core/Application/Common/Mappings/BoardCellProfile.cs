﻿using Application.BoardCells;
using AutoMapper;
using Domain.Entities.Persistence;

namespace Application.Common.Mappings
{
    public class BoardCellProfile : Profile
    {
        public BoardCellProfile()
        {
            CreateMap<BoardCell, BoardCellDto>();
        }
    }
}
