﻿using Application.Boards;
using AutoMapper;
using Domain.Entities.Persistence;

namespace Application.Common.Mappings
{
    public class BoardProfile : Profile
    {
        public BoardProfile()
        {
            CreateMap<Board, BoardDto>();
        }
    }
}
