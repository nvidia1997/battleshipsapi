﻿using Application.Players;
using AutoMapper;
using Domain.Entities.Persistence;

namespace Application.Common.Mappings
{
    public class PlayerProfile : Profile
    {
        public PlayerProfile()
        {
            CreateMap<Player, PlayerDto>();
        }
    }
}
