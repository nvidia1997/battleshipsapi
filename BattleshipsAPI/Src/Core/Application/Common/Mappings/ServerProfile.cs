﻿using Application.Servers;
using AutoMapper;
using Domain.Entities.Persistence;

namespace Application.Common.Mappings
{
    public class ServerProfile : Profile
    {
        public ServerProfile()
        {
            CreateMap<Server, ShortServerDto>();
        }
    }
}
