﻿using Application.Common.Behaviours;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            Assembly executingAssembly = Assembly.GetExecutingAssembly();

            services.AddAutoMapper(executingAssembly);
            services.AddMediatR(executingAssembly);
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidation<,>));

            InjectFluentValidationValidators(services, executingAssembly);

            return services;
        }

        private static void InjectFluentValidationValidators(IServiceCollection services, Assembly executingAssembly)
        {
            AssemblyScanner.FindValidatorsInAssembly(executingAssembly)
           .ForEach(result =>
           {
               services.AddScoped(result.InterfaceType, result.ValidatorType);
           });
        }
    }
}
