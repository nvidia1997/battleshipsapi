﻿namespace Application.Errors
{
    public class ErrorDto
    {
        public string Code { get; set; }
        public string Message { get; set; }

        public ErrorDto()
        {
        }

        public ErrorDto(string code, string message)
        {
            this.Code = code;
            this.Message = message;
        }
    }
}
