﻿using System.Collections.Generic;

namespace Application.Errors
{
    public class ErrorVm
    {
        public IEnumerable<ErrorDto> Errors { get; set; }
    }
}
