﻿using MediatR;

namespace Application.Players.Commands.JoinServer
{
    public class JoinServerCommand : IRequest
    {
        public int ServerId { get; set; }
    }
}
