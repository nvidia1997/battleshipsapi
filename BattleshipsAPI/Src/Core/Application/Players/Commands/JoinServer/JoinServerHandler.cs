﻿using Application.Boards.Constants;
using Application.Common.Exceptions;
using Application.Common.Interfaces.Hubs;
using Application.Common.Interfaces.Infrastructure.Infrastructure.Services;
using Application.Common.Interfaces.Infrastructure.Persistence;
using Application.Players.Commands.JoinServer;
using Application.Servers;
using Application.Servers.Queries.GetShortServer;
using AutoMapper;
using Domain.Entities.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Commands.JoinBoard
{
    public class JoinServerHandler : IRequestHandler<JoinServerCommand, Unit>
    {
        private readonly IPersistenceUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;
        private readonly IBattleshipHub _battleshipHub;
        private readonly IMediator _mediator;

        public JoinServerHandler(
            IPersistenceUnitOfWork unitOfWork,
            IMapper mapper,
            ICurrentUserService currentUserService,
            IBattleshipHub battleshipHub,
            IMediator mediator
            )
        {
            this._unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this._currentUserService = currentUserService ?? throw new ArgumentNullException(nameof(currentUserService));
            this._battleshipHub = battleshipHub ?? throw new ArgumentNullException(nameof(battleshipHub));
            this._mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public async Task<Unit> Handle(JoinServerCommand request, CancellationToken cancellationToken)
        {
            var server = _unitOfWork.Servers.FirstOrDefault(x => x.Id == request.ServerId);

            if (server == null)
            {
                throw new GeneralException(new Errors.ErrorDto { Code = "ServerNotFound" });
            }

            if (server.PlayersCount == 2)
            {
                throw new GeneralException(new Errors.ErrorDto { Code = "ServerIsFull" });
            }

            var applicationUserId = _currentUserService.UserId;

            Player player = _unitOfWork.Players.FirstOrDefault(x => x.Id == applicationUserId);
            if (player == null)
            {
                player = new Player(applicationUserId, _currentUserService.Username);
                _unitOfWork.Players.Add(player);
            }
            else
            {
                player.IsLoser = false;
                if (player.Board != null)
                {
                    _unitOfWork.Boards.Remove(player.Board);
                }
            }
            player.Server = server;

            if (server.PlayersCount == 0)
            {
                player.IsAllowedToShoot = true; // the first player who joins the server makes the first shot
            }

            var board = new Board(BoardConstants.boardSize);
            var cells = board.GenerateCells();
            _unitOfWork.BoardCells.AddRange(cells.ToArray());
            board.Cells = cells;
            board.RandomizeShipLocations();

            _unitOfWork.Boards.Add(board);
            player.Board = board;
            await _unitOfWork.Complete(); //to save newly created entities

            server.PlayersCount = _unitOfWork.Players.Count(x => x.Server == server);

            await _unitOfWork.Complete();// to save players count

            var players = _unitOfWork.Players.FindWithAllProps(x => x.Server == server);

            var serverVM = new ServerVm(server.Id)
            {
                Players = _mapper.Map<List<PlayerDto>>(players),
            };

            await _battleshipHub.JoinServer(server.Id);

            var shortServerVm = await _mediator.Send(new GetShortServerQuery() { ServerId = serverVM.ServerId });
            await _battleshipHub.EmitServersUpdated(serverVM, shortServerVm);

            return Unit.Value;
        }
    }
}
