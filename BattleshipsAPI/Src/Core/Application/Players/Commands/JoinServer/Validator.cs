﻿using FluentValidation;

namespace Application.Players.Commands.JoinServer
{
    public class Validator : AbstractValidator<JoinServerCommand>
    {
        public Validator()
        {
            RuleFor(x => x.ServerId)
                .NotNull();
        }
    }
}
