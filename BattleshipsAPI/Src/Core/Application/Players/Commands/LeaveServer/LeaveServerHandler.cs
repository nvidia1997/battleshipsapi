﻿using Application.Common.Interfaces.Hubs;
using Application.Common.Interfaces.Infrastructure.Infrastructure.Services;
using Application.Common.Interfaces.Infrastructure.Persistence;
using Application.Servers;
using Application.Servers.Queries.GetShortServer;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Commands.LeaveServer
{
    public class LeaveServerHandler : IRequestHandler<LeaveServerCommand, Unit>
    {
        private readonly IPersistenceUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;
        private readonly IBattleshipHub _battleshipHub;
        private readonly IMediator _mediator;

        public LeaveServerHandler(
            IPersistenceUnitOfWork unitOfWork,
            IMapper mapper,
            ICurrentUserService currentUserService,
            IBattleshipHub battleshipHub,
            IMediator mediator
            )
        {
            this._unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this._currentUserService = currentUserService ?? throw new ArgumentNullException(nameof(currentUserService));
            this._battleshipHub = battleshipHub ?? throw new ArgumentNullException(nameof(battleshipHub));
            this._mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public async Task<Unit> Handle(LeaveServerCommand request, CancellationToken cancellationToken)
        {
            var player = _unitOfWork.Players.FirstOrDefaultWithAllProps(u => u.Id == _currentUserService.UserId);

            if (player == null || player.Server == null) { return default; }

            if (player.Board != null)
            {
                _unitOfWork.Boards.Remove(player.Board);
            }

            var server = player.Server;
            _unitOfWork.Players.Remove(player);
            await _unitOfWork.Complete();

            var players = _unitOfWork.Players.FindWithAllProps(x => x.Server == server);
            var playersCount = players.Count();

            if (players.Any(x => x.IsLoser))
            {
                _unitOfWork.Servers.Remove(server);
                server = null;
            }
            else
            {
                server.PlayersCount = playersCount;
            }

            await _unitOfWork.Complete();

            var serverVM = new ServerVm(server.Id)
            {
                Players = server != null ? _mapper.Map<List<PlayerDto>>(players) : null,
            };

            if (serverVM.Players == null)
            {
                await _battleshipHub.EmitServerDeleted(serverVM);
            }

            var shortServerVm = await _mediator.Send(new GetShortServerQuery() { ServerId = serverVM.ServerId });
            await _battleshipHub.EmitServersUpdated(serverVM, shortServerVm);
            await _battleshipHub.LeaveServer(serverVM.ServerId);

            return Unit.Value;
        }
    }
}
