﻿using MediatR;

namespace Application.Players.Commands.Shoot
{
    public class ShootCommand : IRequest
    {
        public int CellId { get; set; }
    }
}
