﻿using Application.Common.Exceptions;
using Application.Common.Interfaces.Hubs;
using Application.Common.Interfaces.Infrastructure.Infrastructure.Services;
using Application.Common.Interfaces.Infrastructure.Persistence;
using Application.Servers;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Players.Commands.Shoot
{
    public class ShootHandler : IRequestHandler<ShootCommand, Unit>
    {
        private readonly IPersistenceUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentUserService _currentUserService;
        private readonly IBattleshipHub _battleshipHub;

        public ShootHandler(
            IPersistenceUnitOfWork unitOfWork,
            IMapper mapper,
            ICurrentUserService currentUserService,
            IBattleshipHub battleshipHub
            )
        {
            this._unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this._currentUserService = currentUserService ?? throw new ArgumentNullException(nameof(currentUserService));
            this._battleshipHub = battleshipHub ?? throw new ArgumentNullException(nameof(battleshipHub));
        }

        public async Task<Unit> Handle(ShootCommand request, CancellationToken cancellationToken)
        {
            var shooter = _unitOfWork.Players.FirstOrDefaultWithAllProps(x => x.Id == _currentUserService.UserId);
            if (shooter.Server.PlayersCount < 2)
            {
                throw new GeneralException(new Errors.ErrorDto { Code = "NotEnoughPlayers" });
            }

            if (!shooter.IsAllowedToShoot)
            {
                throw new GeneralException(new Errors.ErrorDto { Code = "SequentialShooting" });
            }

            var boardCell = _unitOfWork.BoardCells.FirstOrDefault(x => x.Id == request.CellId);
            if (boardCell == null || boardCell.IsHit || boardCell.IsMiss)
            {
                throw new GeneralException(new Errors.ErrorDto { Code = "InvalidBoardCell" });
            }

            if (boardCell.IsShipPart)
            {
                boardCell.IsHit = true;
            }
            else
            {
                boardCell.IsMiss = true;
            }

            shooter.IsAllowedToShoot = false;

            var opponent = _unitOfWork.Players.FirstOrDefaultWithAllProps(x => x.Server == shooter.Server && x.Id != shooter.Id);
            opponent.IsAllowedToShoot = true;

            shooter.IsLoser = shooter.Board.Cells.Where(x => x.IsShipPart).All(x => x.IsHit);
            opponent.IsLoser = opponent.Board.Cells.Where(x => x.IsShipPart).All(x => x.IsHit);

            var players = _unitOfWork.Players.FindWithAllProps(x => x.Server == shooter.Server);

            var vm = new ServerVm(shooter.Server.Id)
            {
                Players = _mapper.Map<List<PlayerDto>>(players),
            };

            await _unitOfWork.Complete();

            await _battleshipHub.EmitServerUpdated(vm);

            return Unit.Value;
        }
    }
}
