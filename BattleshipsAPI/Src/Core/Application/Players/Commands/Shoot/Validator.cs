﻿using FluentValidation;

namespace Application.Players.Commands.Shoot
{
    public class Validator : AbstractValidator<ShootCommand>
    {
        public Validator()
        {
            RuleFor(x => x.CellId)
              .NotNull();
        }
    }
}
