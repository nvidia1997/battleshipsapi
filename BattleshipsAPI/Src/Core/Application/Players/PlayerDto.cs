﻿using Application.Boards;

namespace Application.Players
{
    public class PlayerDto
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public BoardDto Board { get; set; }
        public bool IsAllowedToShoot { get; set; }
        public bool IsLoser { get; set; }
    }
}
