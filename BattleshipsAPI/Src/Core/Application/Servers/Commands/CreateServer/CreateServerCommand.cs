﻿using MediatR;

namespace Application.Servers.Commands.CreateServer
{
    public class CreateServerCommand : IRequest<ShortServerVm>
    {
        public string ServerName { get; set; }
    }
}
