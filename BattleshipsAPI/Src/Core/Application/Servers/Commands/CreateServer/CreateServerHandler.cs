﻿using Application.Common.Exceptions;
using Application.Common.Interfaces.Hubs;
using Application.Common.Interfaces.Infrastructure.Persistence;
using AutoMapper;
using Domain.Entities.Persistence;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Servers.Commands.CreateServer
{
    public class CreateServerHandler : IRequestHandler<CreateServerCommand, ShortServerVm>
    {
        private readonly IPersistenceUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IBattleshipHub _battleshipHub;

        public CreateServerHandler(IPersistenceUnitOfWork unitOfWork, IMapper mapper, IBattleshipHub battleshipHub)
        {
            this._unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this._mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this._battleshipHub = battleshipHub ?? throw new ArgumentNullException(nameof(battleshipHub));
        }

        public async Task<ShortServerVm> Handle(CreateServerCommand request, CancellationToken cancellationToken)
        {
            if (_unitOfWork.Servers.Contains(x => x.Name == request.ServerName))
            {
                throw new GeneralException(new Errors.ErrorDto { Code = "DuplicatedServer" });
            }

            var server = new Server(request.ServerName);

            _unitOfWork.Servers.Add(server);
            await _unitOfWork.Complete();

            var vm = new ShortServerVm()
            {
                Server = _mapper.Map<ShortServerDto>(server),
            };

            await _battleshipHub.EmitServerCreated(vm);

            return vm;
        }
    }
}
