﻿using FluentValidation;

namespace Application.Servers.Commands.CreateServer
{
    public class Validator : AbstractValidator<CreateServerCommand>
    {
        public Validator()
        {
            RuleFor(x => x.ServerName)
                .NotEmpty()
                .WithMessage($"Server's Name is not valid");
        }
    }
}
