﻿using Application.Common.Exceptions;
using Application.Common.Interfaces.Infrastructure.Infrastructure.Services;
using Application.Common.Interfaces.Infrastructure.Persistence;
using Application.Players;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Servers.Queries.GetServer
{
    public class GetServerHandler : IRequestHandler<GetServerQuery, ServerVm>
    {
        private readonly IPersistenceUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        private readonly ICurrentUserService currentUserService;

        public GetServerHandler(IPersistenceUnitOfWork unitOfWork, IMapper mapper, ICurrentUserService currentUserService)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            this.currentUserService = currentUserService ?? throw new ArgumentNullException(nameof(currentUserService));
        }

        public Task<ServerVm> Handle(GetServerQuery request, CancellationToken cancellationToken)
        {
            var players = unitOfWork.Players.FindWithAllProps(x => x.Server.Id == request.ServerId);

            if (!players.Any(x => x.Id == currentUserService.UserId))
            {
                throw new GeneralException();
            }

            var vm = new ServerVm(request.ServerId)
            {
                Players = mapper.Map<List<PlayerDto>>(players),
            };

            return Task.FromResult(vm);
        }
    }
}
