﻿using MediatR;

namespace Application.Servers.Queries.GetServer
{
    public class GetServerQuery : IRequest<ServerVm>
    {
        public int ServerId { get; set; }
    }
}
