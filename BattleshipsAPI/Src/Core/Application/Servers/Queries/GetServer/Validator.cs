﻿using FluentValidation;

namespace Application.Servers.Queries.GetServer
{
    public class Validator : AbstractValidator<GetServerQuery>
    {
        public Validator()
        {
            RuleFor(x => x.ServerId)
                .NotNull();
        }
    }
}
