﻿using Application.Common.Interfaces.Infrastructure.Persistence;
using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Servers.Queries.GetServerList
{
    public class GetServerListHandler : IRequestHandler<GetServerListQuery, ServerListVm>
    {
        private readonly IPersistenceUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public GetServerListHandler(IPersistenceUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<ServerListVm> Handle(GetServerListQuery request, CancellationToken cancellationToken)
        {
            var servers = unitOfWork.Servers.GetAll();

            var vm = new ServerListVm()
            {
                Servers = mapper.Map<List<ShortServerDto>>(servers),
            };

            return Task.FromResult(vm);
        }
    }
}
