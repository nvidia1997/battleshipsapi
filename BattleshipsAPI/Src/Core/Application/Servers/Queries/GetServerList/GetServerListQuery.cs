﻿using MediatR;

namespace Application.Servers.Queries.GetServerList
{
    public class GetServerListQuery : IRequest<ServerListVm>
    {
    }
}
