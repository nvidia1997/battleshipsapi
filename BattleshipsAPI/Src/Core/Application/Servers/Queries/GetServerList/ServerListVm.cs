﻿using System.Collections.Generic;

namespace Application.Servers.Queries.GetServerList
{
    public class ServerListVm
    {
        public List<ShortServerDto> Servers { get; set; }
    }
}
