﻿using Application.Common.Interfaces.Infrastructure.Persistence;
using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Servers.Queries.GetShortServer
{
    public class GetShortServerHandler : IRequestHandler<GetShortServerQuery, ShortServerVm>
    {
        private readonly IPersistenceUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public GetShortServerHandler(IPersistenceUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public Task<ShortServerVm> Handle(GetShortServerQuery request, CancellationToken cancellationToken)
        {
            var server = unitOfWork.Servers.FirstOrDefault(x => x.Id == request.ServerId);

            var vm = new ShortServerVm()
            {
                Server = mapper.Map<ShortServerDto>(server)
            };

            return Task.FromResult(vm);
        }
    }
}
