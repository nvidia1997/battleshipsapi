﻿using MediatR;

namespace Application.Servers.Queries.GetShortServer
{
    public class GetShortServerQuery : IRequest<ShortServerVm>
    {
        public int ServerId { get; set; }
    }
}
