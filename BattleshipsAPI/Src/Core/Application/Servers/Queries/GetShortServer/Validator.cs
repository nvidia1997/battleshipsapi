﻿using FluentValidation;

namespace Application.Servers.Queries.GetShortServer
{
    public class Validator : AbstractValidator<GetShortServerQuery>
    {
        public Validator()
        {
            RuleFor(x => x.ServerId)
                .NotNull();
        }
    }
}
