﻿using Application.Players;
using System.Collections.Generic;

namespace Application.Servers
{
    public class ServerVm
    {
        public int ServerId { get; set; }
        public List<PlayerDto> Players { get; set; }

        public ServerVm(int serverId)
        {
            this.ServerId = serverId;
        }
    }
}
