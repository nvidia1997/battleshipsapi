﻿namespace Application.Servers
{
    public class ShortServerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PlayersCount { get; set; } = 0;
    }
}
