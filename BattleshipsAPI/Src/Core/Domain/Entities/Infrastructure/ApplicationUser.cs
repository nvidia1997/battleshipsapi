﻿using Microsoft.AspNetCore.Identity;

namespace Domain.Entities.Infrastructure
{
    public class ApplicationUser : IdentityUser
    {
    }
}
