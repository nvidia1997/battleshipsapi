﻿using System.Collections.Generic;

namespace Domain.Entities.Persistence
{
    public class Board
    {
        public int Id { get; set; }
        public int Size { get; set; }
        public virtual List<BoardCell> Cells { get; set; }

        public Board(int size)
        {
            this.Size = size;
        }
    }
}
