﻿namespace Domain.Entities.Persistence
{
    public class BoardCell
    {
        public int Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public bool IsHit { get; set; }
        public bool IsMiss { get; set; }
        public bool IsShipPart { get; set; }
    }
}
