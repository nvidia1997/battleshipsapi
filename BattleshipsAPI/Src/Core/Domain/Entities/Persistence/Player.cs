﻿namespace Domain.Entities.Persistence
{
    public class Player
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public virtual Board Board { get; set; }
        public virtual Server Server { get; set; }
        public bool IsAllowedToShoot { get; set; }
        public bool IsLoser { get; set; }

        public Player(string id, string username)
        {
            this.Id = id;
            this.Username = username;
        }
    }
}
