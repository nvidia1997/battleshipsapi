﻿namespace Domain.Entities.Persistence
{
    public class Server
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PlayersCount { get; set; }

        public Server(string name)
        {
            this.Name = name;
        }
    }
}
