﻿using Application.Common.Interfaces.Infrastructure.Persistence;
using Domain.Entities.Persistence;
using Microsoft.EntityFrameworkCore;

namespace Persistense
{
    public class BattleshipDbContext : DbContext, IBattleshipDbContext
    {
        public DbSet<Board> Boards { get; set; }
        public DbSet<BoardCell> BoardCells { get; set; }
        public DbSet<Server> Servers { get; set; }
        public DbSet<Player> Players { get; set; }

        public BattleshipDbContext(DbContextOptions<BattleshipDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(BattleshipDbContext).Assembly);
        }
    }
}
