﻿using Application.Common.Interfaces.Infrastructure.Persistence;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Persistense
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services)
        {
            services.AddDbContext<BattleshipDbContext>(
                options => options.UseInMemoryDatabase(databaseName: "battleshipsDb"),
                ServiceLifetime.Scoped
                );

            services.AddDbContext<IBattleshipDbContext, BattleshipDbContext>(ServiceLifetime.Scoped);

            services.AddScoped<IPersistenceUnitOfWork, PersistenceUnitOfWork>();

            return services;
        }
    }
}
