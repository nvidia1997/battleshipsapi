﻿using Application.Common.Interfaces.Infrastructure.Persistence;
using Application.Common.Interfaces.Infrastructure.Persistence.Repositories;
using Persistense.Repositories;
using System.Threading.Tasks;

namespace Persistense
{
    public class PersistenceUnitOfWork : IPersistenceUnitOfWork
    {
        private readonly BattleshipDbContext context;

        public IBoardsRepository Boards { get; }
        public IBoardCellsRepository BoardCells { get; }
        public IPlayersRepository Players { get; }
        public IServersRepository Servers { get; }

        public PersistenceUnitOfWork(BattleshipDbContext context)
        {
            this.context = context;
            Boards = new BoardsRepository(context);
            BoardCells = new BoardCellsRepository(context);
            Players = new PlayersRepository(context);
            Servers = new ServersRepository(context);
        }

        public Task<int> Complete()
        {
            return context.SaveChangesAsync();
        }

        public void Dispose()
        {
            context.DisposeAsync();
        }
    }
}
