﻿using Application.Common.Interfaces.Infrastructure.Persistence.Repositories;
using Domain.Entities.Persistence;

namespace Persistense.Repositories
{
    public class BoardCellsRepository : PersistenceRepository<BoardCell>, IBoardCellsRepository
    {
        public BoardCellsRepository(BattleshipDbContext context) : base(context)
        {
        }
    }
}
