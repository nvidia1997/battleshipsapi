﻿using Application.Common.Interfaces.Infrastructure.Persistence.Repositories;
using Domain.Entities.Persistence;

namespace Persistense.Repositories
{
    public class BoardsRepository : PersistenceRepository<Board>, IBoardsRepository
    {
        public BoardsRepository(BattleshipDbContext context) : base(context)
        {
        }
    }
}
