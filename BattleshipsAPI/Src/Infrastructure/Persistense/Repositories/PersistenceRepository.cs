﻿using Application.Common.Interfaces.Infrastructure.Persistence.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Persistense.Repositories
{
    public class PersistenceRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        private readonly DbContext context;

        private DbSet<TEntity> DbSet => this.context?.Set<TEntity>();

        public PersistenceRepository(BattleshipDbContext context)
        {
            this.context = context;
        }

        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void AddRange(TEntity[] entities)
        {
            DbSet.AddRange(entities);
        }

        public bool Contains(Func<TEntity, bool> predicate)
        {
            return DbSet.FirstOrDefault(predicate) != null;
        }

        public IQueryable<TEntity> Find(Func<TEntity, bool> predicate, params Expression<Func<TEntity, object>>[] properties)
        {
            return properties
               .Aggregate(DbSet.AsQueryable(), (currentState, property) => currentState.Include(property))
               .Where(predicate)
               .AsQueryable();
        }

        public TEntity FirstOrDefault(Func<TEntity, bool> predicate, params Expression<Func<TEntity, object>>[] properties)
        {
            return properties
                .Aggregate(DbSet.AsQueryable(), (currentState, property) => currentState.Include(property))
                .FirstOrDefault(predicate);
        }

        public void Remove(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        public IQueryable<TEntity> GetAll()
        {
            return DbSet.AsQueryable();
        }

        public int Count(Func<TEntity, bool> predicate)
        {
            return DbSet.Where(predicate).Count();
        }

        public void Update(TEntity entity)
        {
            DbSet.Update(entity);
        }
    }
}
