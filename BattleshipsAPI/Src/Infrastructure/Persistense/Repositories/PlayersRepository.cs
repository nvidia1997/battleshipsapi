﻿using Application.Common.Interfaces.Infrastructure.Persistence.Repositories;
using Domain.Entities.Persistence;
using System;
using System.Linq;

namespace Persistense.Repositories
{
    public class PlayersRepository : PersistenceRepository<Player>, IPlayersRepository
    {
        public PlayersRepository(BattleshipDbContext context) : base(context)
        {
        }

        public IQueryable<Player> FindWithAllProps(Func<Player, bool> predicate)
        {
            return this.Find(predicate, x => x.Board, x => x.Board.Cells, x => x.Server);
        }

        public Player FirstOrDefaultWithAllProps(Func<Player, bool> predicate)
        {
            return this.FirstOrDefault(predicate, x => x.Board, x => x.Board.Cells, x => x.Server);
        }
    }
}
