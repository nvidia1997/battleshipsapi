﻿using Application.Common.Interfaces.Infrastructure.Persistence.Repositories;
using Domain.Entities.Persistence;

namespace Persistense.Repositories
{
    public class ServersRepository : PersistenceRepository<Server>, IServersRepository
    {
        public ServersRepository(BattleshipDbContext context) : base(context)
        {
        }
    }
}
