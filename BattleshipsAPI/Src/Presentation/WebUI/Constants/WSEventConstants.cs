﻿namespace WebUI.Constants
{
    public static class WSEventConstants
    {
        public const string ServerCreated = "serverCreated";
        public const string ServerUpdated = "serverUpdated";
        public const string ServerDeleted = "serverDeleted";
        public const string ShortServerUpdated = "shortServerUpdated";
        public const string JoinServer = "joinServer";
        public const string LeaveServer = "leaveServer";
    }
}
