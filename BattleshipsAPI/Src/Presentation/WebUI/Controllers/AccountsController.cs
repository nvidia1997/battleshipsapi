﻿using Application.Accounts.Commands.Login;
using Application.Accounts.Commands.Logout;
using Application.Accounts.Commands.Register;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Route("api/accounts")]
    public class AccountsController : BaseController
    {
        [HttpPost("register")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> Register([FromBody] RegisterCommand registerCommand)
        {
            await Mediator.Send(registerCommand);
            return Ok();
        }

        [HttpPost("login")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<LoginVm>> Login([FromBody] LoginCommand loginCommand)
        {
            var loginVm = await Mediator.Send(loginCommand);
            return Ok(loginVm);
        }

        [HttpPost("logout")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> Logout()
        {
            await Mediator.Send(new LogoutCommand());
            return Ok();
        }
    }
}
