﻿using Application.Players.Commands.JoinServer;
using Application.Players.Commands.LeaveServer;
using Application.Players.Commands.Shoot;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize]
    [Route("api/players")]
    public class PlayersController : BaseController
    {
        [HttpPost("join-server")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        public async Task<ActionResult> JoinServer([FromBody] JoinServerCommand joinServerCommand)
        {
            await Mediator.Send(joinServerCommand);
            return Accepted();
        }

        [HttpPost("leave-server")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        public async Task<ActionResult> LeaveServer()
        {
            await Mediator.Send(new LeaveServerCommand());
            return Accepted();
        }

        [HttpPost("shoot")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        public async Task<ActionResult> Shoot([FromBody] ShootCommand shootCommand)
        {
            await Mediator.Send(shootCommand);
            return Accepted();
        }
    }
}