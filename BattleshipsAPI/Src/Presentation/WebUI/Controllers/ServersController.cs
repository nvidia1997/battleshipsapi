﻿using Application.Servers;
using Application.Servers.Commands.CreateServer;
using Application.Servers.Queries.GetServer;
using Application.Servers.Queries.GetServerList;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace WebUI.Controllers
{
    [Route("api/servers")]
    [Authorize]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class ServersController : BaseController
    {
        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<ServerListVm>> GetServersList()
        {
            var serverListVm = await Mediator.Send(new GetServerListQuery());
            return Ok(serverListVm);
        }

        [HttpGet("{Id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<ServerVm>> GetServer(int id)
        {
            var serverVm = await Mediator.Send(new GetServerQuery { ServerId = id });
            return Ok(serverVm);
        }

        [HttpPost("create")]
        [ProducesResponseType(StatusCodes.Status202Accepted)]
        public async Task<ActionResult> Create([FromBody] CreateServerCommand createServerCommand)
        {
            await Mediator.Send(createServerCommand);
            return Accepted();
        }
    }
}