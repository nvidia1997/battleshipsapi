﻿using Application.Common.Interfaces.Hubs;
using Application.Common.Interfaces.Infrastructure.Infrastructure.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using WebUI.Hubs;
using WebUI.Services;

namespace WebUI
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPresentation(this IServiceCollection services)
        {
            services.AddSingleton<IBattleshipHub, BattleshipHub>();

            services.AddScoped<ICurrentUserService, CurrentUserService>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            return services;
        }
    }
}
