﻿using Application.Common.Exceptions;
using Application.Errors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace WebUI
{
    internal static class ErrorContextExtensions
    {
        internal static async void SendResponseAsync(this ExceptionContext context, GeneralException generalException)
        {
            context.Result = new StatusCodeResult((int)generalException.StatusCode);
            context.HttpContext.Response.ContentType = "application/json";
            context.HttpContext.Response.StatusCode = (int)generalException.StatusCode;
            context.ExceptionHandled = true;

            var result = JsonConvert.SerializeObject(new ErrorVm
            {
                Errors = generalException.Errors,
            });

            await context.HttpContext.Response.WriteAsync(result);
        }
    }
}
