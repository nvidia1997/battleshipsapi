﻿using Application.Common.Exceptions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System.Net;

namespace WebUI.Filters
{
    public class GeneralExceptionsFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            GeneralException generalException;

            if (context.Exception is GeneralException)
            {
                generalException = context.Exception as GeneralException;
            }
            else
            {
                generalException = new GeneralException(new Application.Errors.ErrorDto { Message = context.Exception.Message })
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                };
                var loggerFactory = LoggerFactory.Create((c) => { });
                var logger = loggerFactory.CreateLogger<GeneralExceptionsFilter>();
                logger.LogError(context.Exception.Message);
            }

            context.SendResponseAsync(generalException);
        }
    }
}
