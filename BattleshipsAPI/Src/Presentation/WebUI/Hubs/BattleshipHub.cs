﻿using Application.Common.Interfaces.Hubs;
using Application.Players.Commands.LeaveServer;
using Application.Servers;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;
using WebUI.Constants;

namespace WebUI.Hubs
{
    public class BattleshipHub : Hub, IBattleshipHub
    {
        private readonly IMediator _mediator;

        public BattleshipHub(IMediator mediator)
        {
            this._mediator = mediator;
        }

        public async Task EmitServerCreated(ShortServerVm serverVm)
        {
            await Clients.All.SendAsync(WSEventConstants.ServerCreated, serverVm);
        }

        public async Task JoinServer(int serverId)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, serverId.ToString());
            await Clients.Caller.SendAsync(WSEventConstants.JoinServer);
        }

        public async Task EmitServersUpdated(ServerVm serverVm, ShortServerVm shortServerVm)
        {
            await this.EmitServerUpdated(serverVm);
            await this.EmitShortServerUpdated(shortServerVm);
        }

        public async Task EmitServerDeleted(ServerVm serverVm)
        {
            await Clients.All.SendAsync(WSEventConstants.ServerDeleted, serverVm.ServerId); ;
        }

        public async Task EmitServerUpdated(ServerVm serverVm)
        {
            await Clients.Group(serverVm.ServerId.ToString()).SendAsync(WSEventConstants.ServerUpdated, serverVm);
        }

        public async Task EmitShortServerUpdated(ShortServerVm shortServerVm)
        {
            await Clients.All.SendAsync(WSEventConstants.ShortServerUpdated, shortServerVm);
        }

        public async Task LeaveServer(int serverId)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, serverId.ToString());
            await Clients.Caller.SendAsync(WSEventConstants.LeaveServer);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);
            await _mediator.Send(new LeaveServerCommand());
        }
    }
}
