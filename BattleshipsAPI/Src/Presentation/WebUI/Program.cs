﻿using Infrastructure.Identity;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Persistense;
using System;

namespace WebUI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webHost = CreateWebHostBuilder(args).Build();

            using (var scope = webHost.Services.CreateScope())
            {
                try
                {
                    var services = scope.ServiceProvider;

                    var applicationDbContext = services.GetRequiredService<ApplicationDbContext>();
                    applicationDbContext.Database.Migrate();

                    var battleshipDbContext = services.GetRequiredService<BattleshipDbContext>();
                    battleshipDbContext.Database.Migrate();
                }
                catch (Exception)
                {
                    Console.WriteLine("Unable to migrate or init database");
                }
            }

            webHost.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
