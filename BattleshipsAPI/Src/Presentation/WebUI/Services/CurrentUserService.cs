﻿using Application.Common.Interfaces.Infrastructure.Infrastructure.Services;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;

namespace WebUI.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public string UserId { get; }
        public string Username { get; }
        public string Email { get; }
        public bool IsAuthenticated { get; }

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            var user = httpContextAccessor.HttpContext?.User;

            this.UserId = user?.FindFirstValue(ClaimTypes.NameIdentifier);
            this.Username = user?.FindFirstValue(ClaimTypes.Name);
            this.Email = user?.FindFirstValue(ClaimTypes.Email);
            this.IsAuthenticated = this.UserId != null;
        }
    }
}
